package springweb;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import springweb.repository.mysql.dao.LoginUserDao;
import springweb.repository.mysql.entity.LoginUser;
import springweb.repository.mysql.entity.Resource;
import springweb.repository.mysql.entity.Role;
import springweb.repository.primary.entity.User;
import springweb.repository.primary.mapper.UserMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HelloControllerTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private LoginUserDao loginUserDao;

	public void getHello() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(equalTo("Greetings from Spring Boot!")));
	}

	public void testMp() {


	}

	public void testMpXML() {
		List<User> users = userMapper.getByAccessNum("15099673101");
		for (User user : users) {
			System.out.println(user.getSubscriberInsId());
		}
	}
	@Test
	public void testmysql() {
		List<LoginUser> users= loginUserDao.findAll();
		for (LoginUser loginUser : users) {
			List<Role> roles = loginUser.getRoles();
			for (Role role : roles) {
				List<Resource> resources = role.getResources();
				for (Resource resource : resources) {
					System.out.println(resource.getPath());
				}
			}
		}
		
	}
}