package springweb.repository.secondary.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "td_b_tradetype")
public class TradeType implements Serializable {

	@Id
	@Column(name = "trade_type_code")
	private String tradeTypeCode;

	@Column(name = "trade_type_name")
	private String tradeTypeName;

	@Column(name = "boss_code")
	private String bossCode;

	@Column(name = "PTRADE_TYPE_CODE")
	private String parentTypeCode;

	@Column(name = "remark")
	private String remark;

	@Column(name = "is_select")
	private String isSelect = "1";

	@Column(name = "version_name")
	private String versionName = "服务请求V3.0";

	@Column(name = "version_code")
	private String versionCode = "10";

	@Column(name = "IS_DISPLAY")
	private String isDisplay = "1";

	@Column(name = "IS_ACCEPET")
	private String isAccept = "1";

	@Column(name = "ATTACHE_PLACE")
	private String attachePlace = "1";

	@Column(name = "CHAGE_TYPE")
	private String chageType = "1";

	@Column(name = "TYPE")
	private String type = "1";

	@Column(name = "REMOVE_TAG")
	private String removeTag = "0";

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "END_DATE")
	@Value(value = "2100-12-31 23:59:59")
	private Date endDate = new Date();

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "START_DATE")
	@Value(value = "2011-12-1 00:00:00")
	private Date startDate = new Date();

	@Column(name = "UPDATE_STAFF_ID")
	private String updateStaffId = "0";

	@Column(name = "UPDATE_DEPART_ID")
	private String updateDepartId = "0";

	@Column(name = "UPDATE_CITY_CODE")
	private String updateCityCode = "0";

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "UPDATE_TIME")
	private Date updateTime = new Date();

	@Column(name = "STRADE_TYPE_CODE")
	private String stradeTypeCode = "001";

	@Transient
	private boolean isLeaf = false;
	
	

	public boolean isLeaf() {
		return isLeaf;
	}

	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public String getTradeTypeCode() {
		return tradeTypeCode;
	}

	public void setTradeTypeCode(String tradeTypeCode) {
		this.tradeTypeCode = tradeTypeCode;
	}

	public String getTradeTypeName() {
		return tradeTypeName;
	}

	public void setTradeTypeName(String tradeTypeName) {
		this.tradeTypeName = tradeTypeName;
	}

	public String getBossCode() {
		return bossCode;
	}

	public void setBossCode(String bossCode) {
		this.bossCode = bossCode;
	}

	public String getParentTypeCode() {
		return parentTypeCode;
	}

	public void setParentTypeCode(String parentTypeCode) {
		this.parentTypeCode = parentTypeCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsSelect() {
		return isSelect;
	}

	public void setIsSelect(String isSelect) {
		this.isSelect = isSelect;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(String isDisplay) {
		this.isDisplay = isDisplay;
	}

	public String getIsAccept() {
		return isAccept;
	}

	public void setIsAccept(String isAccept) {
		this.isAccept = isAccept;
	}

	public String getAttachePlace() {
		return attachePlace;
	}

	public void setAttachePlace(String attachePlace) {
		this.attachePlace = attachePlace;
	}

	public String getChageType() {
		return chageType;
	}

	public void setChageType(String chageType) {
		this.chageType = chageType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRemoveTag() {
		return removeTag;
	}

	public void setRemoveTag(String removeTag) {
		this.removeTag = removeTag;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getUpdateStaffId() {
		return updateStaffId;
	}

	public void setUpdateStaffId(String updateStaffId) {
		this.updateStaffId = updateStaffId;
	}

	public String getUpdateDepartId() {
		return updateDepartId;
	}

	public void setUpdateDepartId(String updateDepartId) {
		this.updateDepartId = updateDepartId;
	}

	public String getUpdateCityCode() {
		return updateCityCode;
	}

	public void setUpdateCityCode(String updateCityCode) {
		this.updateCityCode = updateCityCode;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getStradeTypeCode() {
		return stradeTypeCode;
	}

	public void setStradeTypeCode(String stradeTypeCode) {
		this.stradeTypeCode = stradeTypeCode;
	}

}
