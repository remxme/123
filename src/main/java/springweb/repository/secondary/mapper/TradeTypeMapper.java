package springweb.repository.secondary.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import springweb.repository.secondary.entity.TradeType;

@Mapper
public interface TradeTypeMapper {

	@Select(value = "select t.* from td_b_tradetype t where t.ptrade_type_code =#{parentTypeCode}")
	@ResultType(value=TradeType.class)
	public List<TradeType> getTradeTypeByParentId(
			@Param("parentTypeCode") String parentTypeCode);
	
	public HashMap<String, String> selMaxChildCode(String code,boolean exclude99);
	
	public String getChildCount(String code);
}
