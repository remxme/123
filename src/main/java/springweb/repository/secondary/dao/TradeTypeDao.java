package springweb.repository.secondary.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import springweb.repository.secondary.entity.TradeType;

public interface TradeTypeDao extends JpaRepository<TradeType, String> {

	@Query("from TradeType where parentTypeCode=:code and removeTag='0' and sysdate between startDate and endDate order by tradeTypeCode")
	public List<TradeType> getByTradeTypeCode(@Param("code")String tradeTypeCode);
	
	
	
}
