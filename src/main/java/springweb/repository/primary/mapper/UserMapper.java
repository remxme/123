package springweb.repository.primary.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import springweb.repository.primary.entity.User;

/**
 * @author guyue 
 * mybatis 数据操作类
 */
@Mapper
public interface UserMapper {

	@Select("select * from um_subscriber where access_num=#{accessNum}")
	List<Map<String, Object>> findByAccessNum(
			@Param("accessNum") String accessNum);

	/**
	 * 方法名直接对应mapper/UserMapper.xml中select的id
	 * 
	 * @param accessNum
	 * @return
	 */
	public List<User> getByAccessNumTest(String accessNum);
	
	public List<User> getByAccessNum(String accessName);

}
