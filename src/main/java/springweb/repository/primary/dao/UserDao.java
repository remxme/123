package springweb.repository.primary.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import springweb.repository.primary.entity.User;

/**
 * @author guyue Hibernate数据操作类
 */
public interface UserDao extends JpaRepository<User, Serializable> {

	@Override
	public List<User> findAll();

	public List<User> findByAccessNum(String accessNum);

}
