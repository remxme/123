package springweb.repository.primary.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "um_subscriber")
public class User implements Serializable {

	@Id
	@Column(name = "subscriber_ins_id")
	private String subscriberInsId;

	@Column(name = "access_num")
	private String accessNum;

	@Column(name = "cust_id")
	private String custId;

	@Column(name = "open_date")
	private Date openDate;


	public Date getOpenDate() {
		return openDate;
	}

	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getSubscriberInsId() {
		return subscriberInsId;
	}

	public void setSubscriberInsId(String subscriberInsId) {
		this.subscriberInsId = subscriberInsId;
	}

	public String getAccessNum() {
		return accessNum;
	}

	public void setAccessNum(String accessNum) {
		this.accessNum = accessNum;
	}

}
