package springweb.repository.mysql.mapper;

import springweb.repository.mysql.entity.LoginUser;

public interface LoginUserMapper {
	
	
	public LoginUser selectByUserName(String userName) ;

}
