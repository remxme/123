package springweb.repository.mysql.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="d_resource")
public class Resource {
	@Id
	@Column(name="id")
	private String id;
	
	@Column(name="path")
	private String path;
	
	@Column(name="name")
	private String name;
	
	@OneToMany(mappedBy = "resources", targetEntity = Role.class, fetch = FetchType.LAZY)
	private List<Role> roles;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	

}
