package springweb.repository.mysql.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="d_role")
public class Role {
	
	@Id
	@Column(name="id")
	private String id;
	
	@Column(name="name")
	private String name;
	
	
	@OneToMany(targetEntity = Resource.class, fetch = FetchType.EAGER)
	@JoinTable(name = "d_role_resource", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = { @JoinColumn(name = "resource_id") })
	private List<Resource> resources;
	
	@OneToMany(mappedBy = "roles")
	private List<LoginUser> loginUser;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Resource> getResources() {
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}

	public List<LoginUser> getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(List<LoginUser> loginUser) {
		this.loginUser = loginUser;
	}
	
	

}
