package springweb.repository.mysql.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import springweb.repository.mysql.entity.LoginUser;

public interface LoginUserDao extends JpaRepository<LoginUser,Serializable> {
	
	@Query("from LoginUser where userName=:userName and passWord=:passWord and enabled='1' ")
	public LoginUser selectByUserNamePwd(@Param("userName")String userName, @Param("passWord")String passWord);
	
	@Query("from LoginUser where userName=:userName and enabled='1' ")
	public LoginUser selectByUserName(@Param("userName")String userName);

}
