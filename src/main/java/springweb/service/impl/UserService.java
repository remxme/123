package springweb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springweb.repository.primary.dao.UserDao;
import springweb.repository.primary.entity.User;
import springweb.repository.primary.mapper.UserMapper;
import springweb.service.inter.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UserDao userDao;

	public List<User> getByAccessNumTest(String accessNum) {
		return userMapper.getByAccessNumTest(accessNum);
	}

	@Override
	public List<User> getByAccessNumHibernate(String accessNum) {
		return userDao.findByAccessNum(accessNum);
	}
}
