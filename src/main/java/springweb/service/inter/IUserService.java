package springweb.service.inter;

import java.util.List;

import springweb.repository.primary.entity.User;

public interface IUserService {

	public List<User> getByAccessNumTest(String accessNum);
	
	public List<User> getByAccessNumHibernate(String accessNum);

}
