package springweb.service.tradetype.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springweb.repository.secondary.dao.TradeTypeDao;
import springweb.repository.secondary.entity.TradeType;
import springweb.repository.secondary.mapper.TradeTypeMapper;
import springweb.service.tradetype.inter.ITradeTypeService;

@Service
public class TradeTypeService implements ITradeTypeService {


	@Autowired
	private TradeTypeMapper tradeTypeMapper;
	
	@Autowired
	private TradeTypeDao tradeTypeDao;
	
	
	@Override
	public List<TradeType> getTradeTypeByParentId(String parentTypeCode) {
		return tradeTypeDao.getByTradeTypeCode(parentTypeCode);
	}


	@Override
	public void add(TradeType bean) {
		tradeTypeDao.save(bean);		
	}

}
