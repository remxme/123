package springweb.service.tradetype.inter;

import java.util.List;

import springweb.repository.secondary.entity.TradeType;

public interface ITradeTypeService {
	
	public List<TradeType> getTradeTypeByParentId(String parentTypeCode);
	
	public void add(TradeType bean);

}
