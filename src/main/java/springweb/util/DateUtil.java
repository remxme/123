package springweb.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	private final static SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public static String date2String(Date date) {

		return sdf.format(date);
	}
	
	public static Date string2Date(String date) throws ParseException {

		return sdf.parse(date);
	}

}
