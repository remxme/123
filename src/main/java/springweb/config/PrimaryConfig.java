package springweb.config;

import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactoryPrimary", transactionManagerRef = "transactionManagerPrimary", basePackages = { "springweb.repository.primary.dao" })
// 设置Repository所在位置
@MapperScan(basePackages = { "springweb.repository.primary.mapper" }, sqlSessionTemplateRef = "sqlSessionTemplateRefPrimary")
public class PrimaryConfig {

	@Autowired
	@Qualifier("primaryDataSource")
	private DruidDataSource primaryDataSource;

	@Primary
	@Bean(name = "entityManagerPrimary")
	public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
		return entityManagerFactoryPrimary(builder).getObject()
				.createEntityManager();
	}

	@Primary
	@Bean(name = "entityManagerFactoryPrimary")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryPrimary(
			EntityManagerFactoryBuilder builder) {
		return builder.dataSource(primaryDataSource)
				.properties(getVendorProperties(primaryDataSource))
				.packages("springweb.repository.primary.entity") // 设置实体类所在位置
				.persistenceUnit("primaryPersistenceUnit").build();
	}

	@Autowired
	private JpaProperties jpaProperties;

	private Map<String, String> getVendorProperties(DruidDataSource dataSource) {
		return jpaProperties.getHibernateProperties(dataSource);
	}

	@Primary
	@Bean(name = "transactionManagerPrimary")
	public PlatformTransactionManager transactionManagerPrimary(
			EntityManagerFactoryBuilder builder) {
		return new JpaTransactionManager(entityManagerFactoryPrimary(builder)
				.getObject());
	}

	@Bean(name = "sqlSessionTemplateRefPrimary")
	@Primary
	public SqlSessionTemplate testSqlSessionTemplate(
			@Qualifier("sqlSessionFactoryPrimary") SqlSessionFactory sqlSessionFactory)
			throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

	@Bean(name = "sqlSessionFactoryPrimary")
	@Primary
	public SqlSessionFactory testSqlSessionFactory(
			@Qualifier("primaryDataSource") DruidDataSource dataSource)
			throws Exception {
//		MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
		SqlSessionFactoryBean bean =new SqlSessionFactoryBean();
//		bean.setGlobalConfig(globalConfiguration);
		bean.setDataSource(dataSource);
		bean.setMapperLocations(new PathMatchingResourcePatternResolver()
				.getResources("classpath:mybatis/mapper/primary/*.xml"));
		bean.setTypeAliasesPackage("springweb.repository.primary.entity");
		return bean.getObject();
	}
	
//	@Bean(name="globalConfiguration")
//	public GlobalConfiguration globalConfiguration(){
//		//设置mp全局参数
//		GlobalConfiguration config = new GlobalConfiguration();
//		config.setDbColumnUnderline(true);
//		config.setDbType("oracle");
//		config.setRefresh(true);
//		
//		return config;
//	}

	// @Bean(name = "test1DataSource")
	// @ConfigurationProperties(prefix = "spring.datasource.test1")
	// @Primary
	// public DataSource testDataSource() {
	// return DataSourceBuilder.create().build();
	// }

}