package springweb.config;

import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef="entityManagerFactorySecondary",
        transactionManagerRef="transactionManagerSecondary",
        basePackages= { "springweb.repository.secondary.dao" }) //设置Repository所在位置
@MapperScan(basePackages={"springweb.repository.secondary.mapper"},sqlSessionTemplateRef="sqlSessionTemplateRefSecondary")
public class SecondaryConfig {

    @Autowired @Qualifier("secondaryDataSource")
    private DruidDataSource secondaryDataSource;

    @Bean(name = "entityManagerSecondary")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return entityManagerFactorySecondary(builder).getObject().createEntityManager();
    }

    @Bean(name = "entityManagerFactorySecondary")
    public LocalContainerEntityManagerFactoryBean entityManagerFactorySecondary (EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(secondaryDataSource)
                .properties(getVendorProperties(secondaryDataSource))
                .packages("springweb.repository.secondary.entity") //设置实体类所在位置
                .persistenceUnit("secondaryPersistenceUnit")
                .build();
    }

    @Autowired
    private JpaProperties jpaProperties;

    private Map<String, String> getVendorProperties(DruidDataSource dataSource) {
        return jpaProperties.getHibernateProperties(dataSource);
    }

    @Bean(name = "transactionManagerSecondary")
    PlatformTransactionManager transactionManagerSecondary(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactorySecondary(builder).getObject());
    }
    
    @Bean(name = "sqlSessionTemplateRefSecondary")
	public SqlSessionTemplate testSqlSessionTemplate(
			@Qualifier("sqlSessionFactorySecondary") SqlSessionFactory sqlSessionFactory)
			throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

	@Bean(name = "sqlSessionFactorySecondary")
	public SqlSessionFactory testSqlSessionFactory(
			@Qualifier("secondaryDataSource") DruidDataSource dataSource)
			throws Exception {
//		MybatisSqlSessionFactoryBean bean = new MybatisSqlSessionFactoryBean();
		
		SqlSessionFactoryBean bean =new SqlSessionFactoryBean();
//		bean.setGlobalConfig(globalConfiguration);
		bean.setDataSource(dataSource);
		bean.setMapperLocations(new PathMatchingResourcePatternResolver()
				.getResources("classpath:mybatis/mapper/secondary/*.xml"));
		bean.setTypeAliasesPackage("springweb.repository.secondary.entity");
		return bean.getObject();
	}

//	@Bean(name="globalConfiguration")
//	public GlobalConfiguration globalConfiguration(){
//		//设置mp全局参数
//		GlobalConfiguration config = new GlobalConfiguration();
//		config.setDbColumnUnderline(true);
//		config.setDbType("oracle");
//		config.setRefresh(true);
//		return config;
//	}
}