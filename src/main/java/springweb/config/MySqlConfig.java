package springweb.config;

import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactoryMysql", transactionManagerRef = "transactionManagerMysql", basePackages = {
		"springweb.repository.mysql.dao" }) // 设置Repository所在位置
@MapperScan(basePackages = {
		"springweb.repository.mysql.mapper" }, sqlSessionTemplateRef = "sqlSessionTemplateRefMysql")
public class MySqlConfig {

	@Autowired
	@Qualifier("mysqlDataSource")
	private DruidDataSource mysqlDataSource;

	@Bean(name = "entityManagerMysql")
	public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
		return entityManagerFactoryMysql(builder).getObject().createEntityManager();
	}

	@Bean(name = "entityManagerFactoryMysql")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryMysql(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(mysqlDataSource).properties(getVendorProperties(mysqlDataSource))
				.packages("springweb.repository.mysql.entity") // 设置实体类所在位置
				.persistenceUnit("mysqlPersistenceUnit").build();
	}

	@Autowired
	private JpaProperties jpaProperties;

	private Map<String, String> getVendorProperties(DruidDataSource dataSource) {
		return jpaProperties.getHibernateProperties(dataSource);
	}

	@Bean(name = "transactionManagerMysql")
	PlatformTransactionManager transactionManagerSecondary(EntityManagerFactoryBuilder builder) {
		return new JpaTransactionManager(entityManagerFactoryMysql(builder).getObject());
	}

	@Bean(name = "sqlSessionTemplateRefMysql")
	public SqlSessionTemplate testSqlSessionTemplate(
			@Qualifier("sqlSessionFactoryMysql") SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

	@Bean(name = "sqlSessionFactoryMysql")
	public SqlSessionFactory testSqlSessionFactory(@Qualifier("mysqlDataSource") DruidDataSource dataSource)
			throws Exception {
		// MybatisSqlSessionFactoryBean bean = new
		// MybatisSqlSessionFactoryBean();

		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
		// bean.setGlobalConfig(globalConfiguration);
		bean.setDataSource(dataSource);
		bean.setMapperLocations(
				new PathMatchingResourcePatternResolver().getResources("classpath:mybatis/mapper/mysql/*.xml"));
		bean.setTypeAliasesPackage("springweb.repository.mysql.entity");
		return bean.getObject();
	}

	// @Bean(name="globalConfiguration")
	// public GlobalConfiguration globalConfiguration(){
	// //设置mp全局参数
	// GlobalConfiguration config = new GlobalConfiguration();
	// config.setDbColumnUnderline(true);
	// config.setDbType("oracle");
	// config.setRefresh(true);
	// return config;
	// }
}