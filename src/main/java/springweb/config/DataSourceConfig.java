package springweb.config;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;

/**
 * @author 程序猿DD
 * @version 1.0.0
 * @date 16/3/26 下午9:11.
 * @blog http://blog.didispace.com
 */
@Configuration
public class DataSourceConfig {
	@Primary
	@Bean(name = "primaryDataSource")
	@Qualifier("primaryDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.primary")
	public DruidDataSource primaryDataSource() throws SQLException {
		// return DruidDataSourceBuilder.create().build();
		DruidDataSource primaryData = DruidDataSourceBuilder.create().build();
		primaryData.setFilters("stat");
		return primaryData;
	}

	@Bean(name = "secondaryDataSource")
	@Qualifier("secondaryDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.secondary")
	public DruidDataSource secondaryDataSource() throws SQLException {
		// return DruidDataSourceBuilder.create().build();
		DruidDataSource secondaryData = DruidDataSourceBuilder.create().build();
		secondaryData.setFilters("stat");
		return secondaryData;
	}

	
	@Bean(name = "mysqlDataSource")
	@Qualifier("mysqlDataSource")
	@ConfigurationProperties(prefix = "spring.datasource.mysql")
	public DruidDataSource mysqlDataSource() throws SQLException {
		// return DruidDataSourceBuilder.create().build();
		DruidDataSource dataSource = DruidDataSourceBuilder.create().build();
		dataSource.setFilters("stat");
		return dataSource;
	}
}
