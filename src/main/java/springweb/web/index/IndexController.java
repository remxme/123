package springweb.web.index;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import springweb.config.ShiroDbRealm;
import springweb.repository.mysql.mapper.LoginUserMapper;

@Controller
public class IndexController {
	@Autowired
	private LoginUserMapper loginUserMapper;

	private static final Logger log = Logger.getLogger(IndexController.class);

	private static final org.slf4j.Logger logslf = org.slf4j.LoggerFactory.getLogger(IndexController.class);

	@RequestMapping(value = "/index")
	public ModelAndView index() {
		return new ModelAndView("index", "message", "测试");
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView("login", "message", "测试");
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout() {

		SecurityUtils.getSubject().logout();
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView loginAction(@RequestParam(name = "rememberMe", required = false) String rememberMe,
			@RequestParam("username") String userName, @RequestParam("password") String passWord) {
		ModelAndView modelView = new ModelAndView();
		log.debug("测试=======================");
		logslf.info("111111111111111111");
		// Md5Hash md5 = new Md5Hash(passWord);
		// passWord = md5.toString();
//		SimpleHash sHash = new SimpleHash("MD5", passWord);
		Subject currentUser = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(userName, passWord);

		if (StringUtils.isNotBlank(rememberMe)) {
			token.setRememberMe(true);
		} else {
			token.setRememberMe(false);
		}

		try {
			currentUser.login(token);
			// User user = userService.findByUserName(username);
			// ShiroSession.setUser(user);
			// // currentUser.getSession().setAttribute("user", user);
			// Set<Role> rs = user.getRoles();
			// for (Role role : rs) {
			// System.out.println(role.getName());
			// Set<Resource> rss = role.getResources();
			// for (Resource resource : rss) {
			// System.out.println(resource.getPath());
			// }
			// }
			modelView.addObject("message", "hello");
			// 改变浏览器地址
			modelView.setViewName("index");
		} catch (UnknownAccountException e) {
			modelView.addObject("message", "用户不存在");
			modelView.setViewName("login");
		} /*catch (IncorrectCaptchaException e) {
			modelView.addObject("message", "验证码错误");
			modelView.setViewName("/user/login");
		}*/ catch (AuthenticationException e) {
			modelView.addObject("message", "密码错误");
			modelView.setViewName("/login");
		}

		return modelView;
	}

}
