package springweb.web.tradetype;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import springweb.repository.secondary.entity.TradeType;
import springweb.repository.secondary.mapper.TradeTypeMapper;
import springweb.service.tradetype.inter.ITradeTypeService;
import springweb.util.DateUtil;

@Controller
@RequestMapping(value = "/tradetype")
public class TradeTypeController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ITradeTypeService tradeTypeService;

	@Autowired
	private TradeTypeMapper tradeTypeMapper;

	@RequestMapping(value = "/getTree", method = RequestMethod.GET)
	public String getTradeType() {

		return "tradetype/add";
	}

	@RequestMapping(value = "getTreeNode")
	@ResponseBody
	public List<TradeType> getTreeNode(HttpServletRequest req) {
		logger.info("getTreeNode===" + req.getParameter("tradeTypeCode"));
		String code = req.getParameter("tradeTypeCode") == null ? "201201"
				: req.getParameter("tradeTypeCode");

		List<TradeType> typeList = tradeTypeService
				.getTradeTypeByParentId(code);
		for (TradeType tradeType : typeList) {
			String count = tradeTypeMapper.getChildCount(tradeType.getTradeTypeCode());
			if (Integer.parseInt(count)>0) {
				tradeType.setLeaf(false);
			}else{
				tradeType.setLeaf(true);
			}
		}
		return typeList;
	}

	@RequestMapping(value = "add", method = RequestMethod.POST)
	@ResponseBody
	public String add(HttpServletRequest req,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "currNodeId") String currNodeId,
			@RequestParam(value = "remark") String remark,
			@RequestParam(value = "bossCode") String bossCode,@RequestParam(value = "is99",required=false,defaultValue="") String is99)
			throws ParseException {
		logger.info("currNodeId===" + req.getParameter("currNodeId"));
		HashMap<String, String> val = tradeTypeMapper
				.selMaxChildCode(currNodeId,true);
		
		TradeType r = new TradeType();
		if (is99.equals("on")) {
			r.setTradeTypeCode(String.valueOf(currNodeId) + "299");
			r.setBossCode(bossCode + "99");
		}

		else if (val == null) {
			r.setTradeTypeCode(String.valueOf(currNodeId) + "201");
			r.setBossCode(bossCode + "01");
		} else {
			String code = val.get("TRADE_TYPE_CODE");
			
//			if (code.endsWith("99")) {
//				
//			}
			String pre = code.substring(0, code.length() - 3);
			String end = code.substring(code.length() - 3);

			String boss = val.get("BOSS_CODE");
			String preBoss = boss.substring(0, boss.length() - 2);
			String endBoss = boss.substring(boss.length() - 2);


			// Long maxChild = Long.parseLong(val.get("TRADE_TYPE_CODE"));
			// Long maxBossCode = Long.parseLong(val.get("BOSS_CODE"));
			String sEndBoss = String.valueOf(Integer.parseInt(endBoss) + 1);
			r.setTradeTypeCode(pre + String.valueOf(Integer.parseInt(end) + 1));
			r.setBossCode(preBoss
					+ (sEndBoss.length() == 2 ? sEndBoss : "0" + sEndBoss));
		}
		r.setRemark(remark + "->" + name);
		r.setTradeTypeName(name);
		r.setParentTypeCode(currNodeId);
		r.setStartDate(DateUtil.string2Date("2011-12-1 00:00:00"));
		r.setEndDate(DateUtil.string2Date("2100-12-31 23:59:59"));
		tradeTypeService.add(r);
		return "success";
	}

	public static void main(String[] args) {
		String aa="abcdef";
		System.out.println(aa.substring(0,aa.length()-3));
	}

}
