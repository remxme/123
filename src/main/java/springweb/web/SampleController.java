package springweb.web;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import springweb.repository.mysql.dao.LoginUserDao;
import springweb.repository.mysql.entity.LoginUser;
import springweb.repository.primary.entity.User;
import springweb.repository.primary.mapper.UserMapper;
import springweb.service.impl.UserService;
import springweb.util.DateUtil;

@RestController
public class SampleController {
	@Autowired
	private UserService userService;

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private LoginUserDao loginUserDao;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	String home() {
		List<Map<String, Object>> userList = userMapper
				.findByAccessNum("13659948687");
		StringBuilder sb = new StringBuilder();

		for (Map<String, Object> user : userList) {
			sb.append(user.get("ACCESS_NUM")).append("=")
					.append(user.get("CUST_ID")).append("--");
		}

		List<User> users = userService.getByAccessNumTest("15099673101");

		for (User user : users) {
			sb.append(user.getAccessNum()).append("=").append(user.getCustId()).append("=")
					.append(DateUtil.date2String(user.getOpenDate()))
					.append("=").append("--");
		}

		return sb.toString();
	}
	@RequestMapping(value="/freeMarker")
	public ModelAndView testFreeMarker(){
		
		List<User> users = userService.getByAccessNumTest("15099673101");
		return new ModelAndView("hello","users",users);
	}
	
	@RequestMapping(value="/hibernate")
	public ModelAndView testHibernate(){
		
		List<User> users = userService.getByAccessNumHibernate("15099673101");
		return new ModelAndView("hello","users",users);
	}
	

	@RequestMapping(value="/mp")
	public ModelAndView testMp(){
		return null;
		
	}
	@RequestMapping(value="/mysql")
	public ModelAndView testMysql(){
		
		List<LoginUser> users = loginUserDao.findAll();
		return new ModelAndView("mysql","users",users);
	}
	

}
